CREATE DATABASE IF NOT EXISTS nextcloud;

--change root passoword
ALTER USER 'root'@'localhost' IDENTIFIED BY 'bbwadmin';

--create admin
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin1234'; 

--admin full access
GRANT ALL PRIVILEGES ON nextcloud.* TO 'admin'@'localhost'; 

--activate changes
FLUSH PRIVILEGES;
